from django.urls import path

from . import views

app_name = 'project_name'

urlpatterns = [
    path('', views.story1, name='story1'),
]
